package com.nico;

import java.awt.event.InputEvent;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("--- AutoClicker ---");
		
		System.out.println("Enter the number of desired clicks: ");
		int clicks = scanner.nextInt();

		System.out.println("Enter delay between clicks in miliseconds: ");
		int delay = scanner.nextInt();
		
		
		// So anxious...
		try {
			System.out.println("Program will start in 3 seconds...");
			Thread.sleep(1000);
			System.out.println("Program will start in 2 seconds...");
			Thread.sleep(1000);
			System.out.println("Program will start in 1 seconds...");
			Thread.sleep(1000);
			System.out.println("Autoclicking!");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Instance and setting delay.
		AutoClicker autoClicker = new AutoClicker();
		autoClicker.setDelay(delay);
		
		// Let the magic begin.
		for (int i = 0 ; i<clicks ; i++) {
			autoClicker.clickMouse(InputEvent.BUTTON1_MASK);
		}
		
		System.out.println("AutoClicker complete.");
		
	}

}
