package com.nico;

import java.awt.AWTException;
import java.awt.Robot;

public class AutoClicker {

	private Robot robot;
	
	// Delay between clicks in ms.
	private int delay;
	
	// Instance robot at constructor.
	public AutoClicker() {
		try {
			robot = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}
	
	//  Method for clicking.
	public void clickMouse(int button) {
		try {
			robot.mousePress(button);
			robot.delay(delay);
			robot.mouseRelease(button);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setDelay(int ms) {
		this.delay = ms;
	}
	
	
}
